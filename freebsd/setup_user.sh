#!/bin/sh

if [ $(id -u) -eq 0 ]; then
    echo "Don't run this script as root!!!"
    exit 1
fi
cp .xinitrc ~/
cp .xsession ~/

cat etc/profile >> ~/profile
sed -i -e 's/EDITOR=vi/EDITOR=nano/g' ~/profile
