#!/bin/sh

if [ $(id -u) -ne 0 ]; then
    echo "You must be root to run this Script!"
    exit 1
fi

cp etc/profile /etc/profile
cp 20-keyboard.conf /usr/local/etc/X11/xorg.conf.d
cp 10-restart.stop.suspend.rules /usr/local/etc/polkit-1/rules.d/10-restart.stop.suspend.rules

echo "proc      /proc       procfs      rw      0 0" >> /etc/fstab
echo << EOF >> /etc/rc.conf

hald_enable="yes"
dbus_enable="yes"

vboxguest_enable="yes"
vboxservice_enable="yes"

sddm_enable="yes"
EOF

pkg install libxfce4gui libxfce4menu libxfce4util xfce4-appfinder xfce4-battery-plugin xfce4-bsdcpufreq-plugin xfce4-calculator-plugin \
xfce4-clipman-plugin xfce4-conf xfce4-cpugraph-plugin xfce4-dashboard xfce4-datetime-plugin xfce4-desktop xfce4-dev-tools \
xfce4-dict-plugin xfce4-diskperf-plugin xfce4-embed-plugin xfce4-equake-plugin xfce4-fsguard-plugin xfce4-generic-slider \
xfce4-genmon-plugin xfce4-goodies xfce4-kbdleds-plugin xfce4-mailwatch-plugin xfce4-mixer xfce4-mount-plugin \
xfce4-mpc-plugin xfce4-netload-plugin xfce4-notes-plugin xfce4-notifyd xfce4-panel xfce4-panel-profiles xfce4-places-plugin \
xfce4-power-manager xfce4-print xfce4-pulseaudio-plugin xfce4-quicklauncher-plugin xfce4-screensaver xfce4-screenshooter-plugin \
xfce4-session xfce4-settings xfce4-smartbookmark-plugin xfce4-stopwatch-plugin xfce4-systemload-plugin xfce4-taskmanager \
xfce4-terminal xfce4-time-out-plugin xfce4-timer-plugin xfce4-tumbler xfce4-verve-plugin xfce4-volumed xfce4-volumed-pulse \
xfce4-wavelan-plugin xfce4-weather-plugin xfce4-whiskermenu-plugin xfce4-wm xfce4-wm-themes xfce4-wmdock-plugin xfce4-xkb-plugin