#!/bin/bash
###############
#	ZFS Install Script
#
# Please Change settings in Disk Setup to your own
# Setup.

HOSTNAME="zubuntu"

######## Disk Setup #########
# /dev/sda
#DISK1=/dev/disk/by-id/ata-VBOX_HARDDISK_VB2941e124-e6f8ae3a
DISK1=/dev/sda

#/dev/sdb
#DISK2=/dev/disk/by-id/ata-VBOX_HARDDISK_VBb289aa2e-2739df61
DISK2=/dev/sdb

####### Boot Pool #####
BOOTPOOL=bpool

####### ZPOOL ########
MAINPOOL=zroot
ROOTPOOL=$MAINPOOL/ROOT
ROOTINSTALL=$ROOTPOOL/ubuntu



function setup_disks
{
    #### Zap all on /dev/sda
    sgdisk --zap-all $DISK1
    if [ $? -ne 0 ]
    then
        echo "Failed to zap-all on ${DISK1}"
        exit 
    fi

    ### Zap all on /dev/sdb
    sgdisk --zap-all $DISK2
    if [ $? -ne 0 ]
    then
        echo "Failed to zap-all on ${DISK2}"
        exit 
    fi

    ### create part 1
    sgdisk -a1 -n1:24k:+1000k -t1:EF002 $DISK1
    if [ $? -ne 0 ]
    then
        echo "Failed create Partition 1 on ${DISK1}"
        exit 
    fi
    sgdisk -a1 -n1:24k:+1000k -t1:EF002 $DISK2
    if [ $? -ne 0 ]
    then
        echo "Failed create Partition 1 on ${DISK2}"
        exit 
    fi

    ### create boot part
    sgdisk -n3:0:+1G -t3:EF02 --change-name=3:"BIOS boot partition" $DISK1
    if [ $? -ne 0 ]
    then
        echo "Failed create Boot Partition on ${DISK1}"
        exit 
    fi
    sgdisk -n3:0:+1G -t3:EF02 --change-name=3:"BIOS boot partition" $DISK2
    if [ $? -ne 0 ]
    then
        echo "Failed create Boot Partition on ${DISK2}"
        exit 
    fi
    
    ## create root part
    sgdisk -n4:0:0 -t4:BF01 $DISK1
    if [ $? -ne 0 ]
    then
        echo "Failed create Root Partition on ${DISK1}"
        exit 
    fi

    sgdisk -n4:0:0 -t4:BF01 $DISK2
    if [ $? -ne 0 ]
    then
        echo "Failed create Boot Partition on ${DISK1}"
        exit 
    fi

    return 0
}

function setup_zfs_pools 
{
    zpool create -o ashift=12 -d \
    -o feature@async_destroy=enabled \
    -o feature@bookmarks=enabled \
    -o feature@embedded_data=enabled \
    -o feature@empty_bpobj=enabled \
    -o feature@enabled_txg=enabled \
    -o feature@extensible_dataset=enabled \
    -o feature@filesystem_limits=enabled \
    -o feature@hole_birth=enabled \
    -o feature@large_blocks=enabled \
    -o feature@lz4_compress=enabled \
    -o feature@spacemap_histogram=enabled \
    -o feature@userobj_accounting=enabled \
    -O acltype=posixacl -O canmount=off -O compression=lz4 -O devices=off \
    -O normalization=formD -O relatime=on -O xattr=sa \
    -O mountpoint=/ -R /mnt  -f $BOOTPOOL mirror ${DISK1}3 ${DISK2}3
    if [ $? -ne 0 ]
    then
        echo "Failed to Create ${BOOTPOOL}"
        exit 
    fi

    zpool create -o ashift=12 \
    -O acltype=posixacl -O canmount=off -O compression=lz4 \
    -O dnodesize=auto -O normalization=formD -O relatime=on -O xattr=sa \
    -O mountpoint=/ -R /mnt -f $MAINPOOL mirror ${DISK1}4 ${DISK2}4
    if [ $? -ne 0 ]
    then
        echo "Failed to Create ${MAINPOOL}"
        exit 
    fi
    
    zfs create -o canmount=off -o mountpoint=none $ROOTPOOL
    if [ $? -ne 0 ]
    then
        echo "Failed to Create ${ROOTPOOL}"
        exit 
    fi

    zfs create -o canmount=off -o mountpoint=none $BOOTPOOL/BOOT
    if [ $? -ne 0 ]
    then
        echo "Failed to Create ${BOOTPOOL}/BOOT"
        exit 
    fi

    zfs create -o canmount=noauto -o mountpoint=/ $ROOTINSTALL
    if [ $? -ne 0 ]
    then
        echo "Failed to Create ${ROOTINSTALL}"
        exit 
    fi
    zfs mount $ROOTINSTALL
    if [ $? -ne 0 ]
    then
        echo "Failed to mount ${ROOTINSTALL}"
        exit 
    fi

    zfs create -o canmount=noauto -o mountpoint=/boot $BOOTPOOL/BOOT/ubuntu
    if [ $? -ne 0 ]
    then
        echo "Failed to Create ${BOOTPOOL}/BOOT/ubuntu"
        exit 
    fi

    zfs mount $BOOTPOOL/BOOT/ubuntu
    if [ $? -ne 0 ]
    then
        echo "Failed to mount ${BOOTPOOL}/BOOT/ubuntu"
        exit 
    fi

    zfs create $MAINPOOL/home
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/home"
        exit 
    fi

    zfs create -o mountpoint=/root/ $MAINPOOL/home/root
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/home/root"
        exit 
    fi

    zfs create -o canmount=off $MAINPOOL/var
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/var"
        exit 
    fi

    zfs create -o canmount=off $MAINPOOL/var/lib
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/var/lib"
        exit 
    fi

    zfs create $MAINPOOL/var/log
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/var/log"
        exit 
    fi

    zfs create $MAINPOOL/var/spool
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/var/spool"
        exit 
    fi

    zfs create -o com.sun:auto-snapshot=false $MAINPOOL/var/cache
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/var/cache"
        exit 
    fi

    zfs create -o com.sun:auto-snapshot=false $MAINPOOL/var/tmp
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/var/tmp"
        exit 
    fi
    chmod 1777 /mnt/var/tmp
    if [ $? -ne 0 ]
    then
        echo "Failed to chmod ${MAINPOOL}/var/tmp"
        exit 
    fi

    zfs create $MAINPOOL/opt
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/opt"
        exit 
    fi

    zfs create $MAINPOOL/srv
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/srv"
        exit 
    fi

    zfs create -o canmount=off $MAINPOOL/usr
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/usr"
        exit 
    fi

    zfs create $MAINPOOL/usr/local
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/usr/local"
        exit 
    fi

    zfs create $MAINPOOL/var/mail
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/var/mail"
        exit 
    fi

    zfs create $MAINPOOL/var/snap
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/var/snap"
        exit 
    fi

    zfs create $MAINPOOL/var/www
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/var/www"
        exit 
    fi

    zfs create $MAINPOOL/var/lib/AccountService
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/var/lib/AccountService"
        exit 
    fi

    zfs create -o com.sun:auto-snapshot=false $MAINPOOL/tmp
    if [ $? -ne 0 ]
    then
        echo "Failed to create ${MAINPOOL}/tmp"
        exit 
    fi
    chmod 1777 /mnt/tmp
    if [ $? -ne 0 ]
    then
        echo "Failed to chmod ${MAINPOOL}/tmp"
        exit 
    fi
    
    return 0
}

function create_repository 
{
    echo << EOF >> /mnt/etc/apt/source.list
deb http://archive.ubuntu.com/ubuntu/ eoan main universe
deb-src http://archive.ubuntu.com/ubuntu/ eoan main universe

deb http://security.ubuntu.com/ubuntu/ eoan-security main universe
deb-src http://security.ubuntu.com/ubuntu/ eoan-security main universe

deb http://archive.ubuntu.com/ubuntu/ eoan-updates main universe
deb-src http://archive.ubuntu.com/ubuntu/ eoan-updates main universe
EOF
    if [ $? -ne 0 ]
    then
        echo "Failed to set up Apt source.list"
        exit
    fi
    return 0
}

apt-add-repository universe
apt update

apt install --yes debootstrap gdisk zfs-initramfs
if [ $? -ne 0 ]
then
    echo "Failed to install debootstrap gdisk zfs-initramfs"
    exit
fi

setup_disks
setup_zfs_pools

debootstrap eoan /mnt
if [ $? -ne 0 ]
    then
        echo "Failed to bootstrap eoan to /mnt"
        exit
    fi

zfs set devices=off $MAINPOOL
if [ $? -ne 0 ]
then
    echo "Failed to set up Apt source.list"
    exit
fi
create_repository

echo $HOSTNAME > /mnt/etc/hostname
echo "127.0.0.1		${HOSTNAME}" >> /mnt/etc/hosts

mount --rbind /dev /mnt/dev
mount --rbind /proc /mnt/proc
mount --rbind /sys /mnt/sys
cp ubuntu-19.10.ubuntu-chroot.sh /mnt/root/
chmod +x /mnt/root/ubuntu-19.10.ubuntu-chroot.sh
cp 10_linux_zfs.patch /mnt/root

echo "type now: chroot /mnt bash --login"
echo "inside your chroot type:"
echo "cd"
echo "./ubuntu-19.10.ubuntu-chroot.sh"
echo "wait until its finished"
