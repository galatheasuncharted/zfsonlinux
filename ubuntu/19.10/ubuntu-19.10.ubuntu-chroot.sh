#!/bin/bash

##########
# Global config
##########

ROOTPOOL=zroot
USER_NAME=gala


ln -s /proc/self/mounts /etc/mtab
apt update
dpkg-reconfigure locales
dpkg-reconfigure tzdata

apt install --yes nano
apt install --yes --no-install-recommends linux-image-generic
apt install --yes zfs-initramfs
apt install --yes grub-pc
echo "now we must set up a new Root Password:"
passwd

cp /usr/share/systemd/tmp.mount /etc/systemd/system/
systemctl enable tmp.mount
addgroup --system lpadmin
addgroup --system sambashare
grub-probe /boot
if [ $? -ne 0 ]
then
  echo "Failed to probe grub on /boot"
  exit 
fi 

# make an empty initramfs file
touch /boot/initrd.img-$(uname -r)

update-initramfs -u -k all

echo << EOF >> /etc/systemd/system/zfs-import-bpool.service
[Unit]
DefaultDependencies=no
Before=zfs-import-scan.service
Before=zfs-import-cache.service

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/sbin/zpool import -N -o cachefile=none bpool

[Install]
WantedBy=zfs-import.target
systemctl enable zfs-import-bpool.service
EOF

cp /usr/share/systemd/tmp.mount /etc/systemd/system
systemctl enable tmp.mount

# Copy the Grup ZFS Patch
cp 10_linux_zfs.patch /etc/grub.d/10_linux_zfs

sed -i 's/GRUB_CMDLINE_LINUX=""/GRUB_CMDLINE_LINUX="root=ZFS=zroot\/ROOT\/ubuntu"/g' /etc/default/grub
sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"/GRUB_CMDLINE_LINUX_DEFAULT=""/g' /etc/default/grub
sed -i 's/GRUB_TIMEOUT_STYLE=hidden/#GRUB_TIMEOUT_STYLE=hidden/g' /etc/default/grub
sed -i 's/GRUB_TIMEOUT=0/GRUB_TIMEOUT=5/g' /etc/default/grub
sed -i 's/#GRUB_TERMINAL=console/GRUB_TERMINAL=console/g' /etc/default/grub

update-grub
grub-install /dev/sda
if [ $? -ne 0 ]
then
  echo "Failed to install grub on /dev/sda"
  exit 
fi 

grub-install /dev/sdb
if [ $? -ne 0 ]
then
  echo "Failed to install grub on /dev/sdb"
  exit 
fi


zfs set mountpoint=legacy bpool/BOOT/ubuntu
echo bpool/BOOT/ubuntu /boot zfs \
nodev,relatime,x-systemd.requires=zfs-import-bpool.service 0 0 >> /etc/fstab

zfs set mountpoint=legacy $ROOTPOOL/var/log
echo $ROOTPOOL/var/log /var/log zfs nodev,relatime 0 0 >> /etc/fstab

zfs set mountpoint=legacy $ROOTPOOL/var/spool
echo $ROOTPOOL/var/spool /var/spool zfs nodev,relatime 0 0 >> /etc/fstab

zfs set mountpoint=legacy $ROOTPOOL/var/tmp
echo $ROOTPOOL/var/tmp /var/tmp zfs nodev,relatime 0 0 >> /etc/fstab

zfs set mountpoint=legacy $ROOTPOOL/tmp
echo $ROOTPOOL/tmp /tmp zfs nodev,relatime 0 0 >> /etc/fstab

zfs snapshot bpool/BOOT/ubuntu@initial_install
zfs snapshot $ROOTPOOL/ROOT/ubuntu@initial_install

zfs create -V 4G -b $(getconf PAGESIZE) -o compression zle \
-o logbias=throughput -o sync=always \
-o primarycache=metadata -o secondarycache=none \
-o com.sun:auto-snapshot=false $ROOTPOOL/swap

mkswap -f /dev/zvol/$ROOTPOOL/swap
echo /dev/zvol/$ROOTPOOL/swap none swap discard 0 0 >> /etc/fstab
echo RESUME=none > /etc/initramfs-tools/conf.d/resume
apt dist-upgrade --yes
apt install --yes ubuntu-desktop

echo << EOF >> /etc/netplan/01-netcfg.yaml
network:
  version: 2
  renderer: NetworkManager
EOF

zfs create $ROOTPOOL/home/$USER_NAME
adduser $USER_NAME
cp -a /etc/skel/. /home/$USER_NAME
chown -R gala:gala /home/$USER_NAME
usermod -a -G adm,cdrom,dip,lpadmin,plugdev,sambashare,sudo $USER_NAME
